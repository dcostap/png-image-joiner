package com.libgdx.my_game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.libgdx.my_game.MyGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = 10;
		config.height = 10;
		config.vSyncEnabled = false;
        config.foregroundFPS = 60;
        config.backgroundFPS = 60;

        config.title = "Image Joiner";

		new LwjglApplication(new MyGame(), config);
	}
}
