package com.libgdx.my_game

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.PixmapIO
import com.badlogic.gdx.utils.Array
import java.io.File
import java.math.BigInteger

class MyGame : Game() {

    override fun create() {
        val parent = File(".")
        for (file in parent.walk()) {
            if (file.parent != parent.name) continue
            if (file.isDirectory) {
                if (file === parent) continue
                println(file.name)
                newJoin(file)
            }
        }

        Gdx.app.exit()
    }

    fun isPixelTransparent(pixel: Int): Boolean {
        val b: Byte = 0
        return BigInteger.valueOf(pixel.toLong()).toByteArray().last() == b
    }

    fun cropIt(image: Pixmap): Pixmap {
        val width = image.width
        val height = image.height

        var offsetX = 0
        var offsetY = 0
        var subImageWidth = width
        var subImageHeight = height

        var minY = height
        var maxY = 0
        var minX = width
        var maxX = 0

        for (xx in 0 until width) {
            for (yy in 0 until height) {
                val pixel = image.getPixel(xx, yy)

                if (!isPixelTransparent(pixel)) {
                    if (yy < minY) minY = yy
                    if (yy > maxY) maxY = yy

                    if (xx < minX) minX = xx
                    if (xx > maxX) maxX = xx
                }
            }
        }

        offsetX = minX - 1
        subImageWidth = Math.max(0, maxX - minX) + 2
        offsetY = minY - 1
        subImageHeight = Math.max(0, maxY - minY) + 2

        if (subImageHeight > height) subImageHeight = height
        if (subImageWidth > width) subImageWidth = width
        if (offsetX < 0) offsetX = 0
        if (offsetY < 0) offsetY = 0

        val newImage = Pixmap(subImageWidth, subImageHeight, Pixmap.Format.RGBA8888)
        newImage.blending = Pixmap.Blending.None
        var isFullyTransparent = true

        for (xx in offsetX until offsetX + subImageWidth) {
            for (yy in offsetY until offsetY + subImageHeight) {
                val pixel = image.getPixel(xx, yy)
                newImage.drawPixel(xx - offsetX, yy - offsetY, pixel)

                if (isFullyTransparent && !isPixelTransparent(pixel)) {
                    isFullyTransparent = false
                }
            }
        }

        return newImage
    }

    fun newJoin(parent: File) {
        val files = Array<File>()
        for (file in parent.walk()) {
            if (file.isFile && file.extension == "png") {
                files.add(file)
            }
        }

        println(files.size)
        if (files.size == 0) return

        val columns = 8
        val maxAllowedWidth = 750
        var maxWidth = 0
        val rowHeight = HashMap<Int, Int>()
        var width = 0

        var x = 0
        var y = 0

        for (file in files) {
            if (!rowHeight.containsKey(y)) {
                rowHeight.put(y, 0)
            }
            val image = cropIt(Pixmap(FileHandle(file)))
            if (rowHeight[y]!! < image.height) rowHeight[y] = image.height
            width += image.width
            if (width > maxWidth) maxWidth = width
            x++
            if (width > maxAllowedWidth) {
                width = 0
                x = 0
                y++
            }
        }

        var maxHeight = 0
        for (h in rowHeight) maxHeight += h.value

        var column = 0
        var row = 0
        var xx = 0
        var yy = 0

        val newImage = Pixmap(maxWidth, maxHeight, Pixmap.Format.RGBA8888)
        newImage.blending = Pixmap.Blending.None

        width = 0
        for (file in files) {
            val image = cropIt(Pixmap(FileHandle(file)))

            width += image.width
            newImage.drawPixmap(image, xx, yy)
            xx += image.width

            column++
            if (width > maxAllowedWidth) {
                width = 0
                column = 0
                xx = 0
                yy += rowHeight[row]!!.toInt()
                row++
            }
        }

        println("output -> " + FileHandle("./" + parent.name + ".png").path())
        PixmapIO.writePNG(FileHandle("./" + parent.name + ".png"), newImage)
    }

    override fun dispose() {
        super.dispose()
    }

    override fun pause() {
        super.pause()
    }

    override fun resume() {
        super.resume()
    }

    override fun render() {
        super.render()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
    }

    override fun setScreen(screen: Screen) {
        super.setScreen(screen)
    }

    override fun getScreen(): Screen {
        return super.getScreen()
    }
}
