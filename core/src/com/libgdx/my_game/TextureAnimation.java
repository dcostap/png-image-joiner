package com.libgdx.my_game;

/**
 * Created by Darius on 17/01/2018
 */
public class TextureAnimation<T> {
    private T[] frames;
    private float elapsedTime = 0;
    private boolean paused = false;
    private float frameDuration;
    private float totalAnimationDuration;
    private Type type;

    private Type normalReversedCycle = Type.NORMAL;

    public enum Type {
        NORMAL, REVERSED, NORMAL_REVERSED_LOOP
    }

    public TextureAnimation(float frameDuration, T[] frames) {
        this.frames = frames;
        this.frameDuration = frameDuration;
        this.type = Type.NORMAL;
        updateTotalAnimationDuration();
    }

    private void updateTotalAnimationDuration() {
        this.totalAnimationDuration = frameDuration * frames.length;
    }

    public T getFrame(float delta) {
        if (!paused) {
            switch (type) {
                case NORMAL:
                    elapsedTime += delta;
                    if (finishedNormalAnimation()) elapsedTime = 0;
                    break;

                case REVERSED:
                    elapsedTime -= delta;
                    if (finishedReversedAnimation()) elapsedTime = totalAnimationDuration - 0.0001f;
                    break;

                case NORMAL_REVERSED_LOOP:
                    if (normalReversedCycle == Type.NORMAL) {
                        elapsedTime += delta;
                        if (finishedNormalAnimation()) {
                            normalReversedCycle = Type.REVERSED;
                            setElapsedTime(getTotalAnimationDuration() - getFrameDuration() - 0.001f);
                        }
                    } else if (normalReversedCycle == Type.REVERSED) {
                        elapsedTime -= delta;
                        if (finishedReversedAnimation()) {
                            normalReversedCycle = Type.NORMAL;
                            setElapsedTime(getFrameDuration());
                        }
                    }
                    break;
            }
        }

        return getFrameFromElapsedTime();
    }

    public boolean finishedNormalAnimation() {
        return (elapsedTime >= totalAnimationDuration);
    }

    public boolean finishedReversedAnimation() {
        return (elapsedTime <= 0);
    }

    private T getFrameFromElapsedTime() {
        int index;
        index = (int) Math.floor(elapsedTime / frameDuration);
        index = Math.max(index, 0);
        return frames[index];
    }

    public void resetAnimation() {
        elapsedTime = 0;
    }

    public void pauseAnimation() {
        paused = true;
    }

    public void resumeAnimation() {
        paused = false;
    }

    public void setFrame(int frameIndex) {
        if (frameIndex < 0 || frameIndex > frames.length - 1) return;

        elapsedTime = getFrameDuration() * frameIndex;
    }

    public boolean isPaused() {
        return paused;
    }

    public float getFrameDuration() {
        return frameDuration;
    }

    public void setFrameDuration(float frameDuration) {
        this.frameDuration = frameDuration;

        updateTotalAnimationDuration();
    }

    public float getTotalAnimationDuration() {
        return totalAnimationDuration;
    }

    public T[] getFrames() {
        return frames;
    }

    public float getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(float elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
